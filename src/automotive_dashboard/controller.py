from dash import Dash
from dash.html import Div
from dash.dcc import Graph
from pandas import DataFrame
from plotly.express import histogram


class DashboardApplication(Dash):
    def __init__(self, data: DataFrame):
        super().__init__("Automotive Dashboard")
        self.layout = Div([Graph(figure=histogram(data, x="Manufacturer"))])
