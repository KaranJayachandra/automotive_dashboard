from pandas import read_csv
from argparse import ArgumentParser, FileType
from automotive_dashboard.controller import DashboardApplication


def main():
    parser = ArgumentParser(
        prog="automotive_dashboard",
        description="Run the dash web applications using the given input data",
        epilog="Author: Karan Jayachandra",
    )
    parser.add_argument(
        "-i",
        "--input_file",
        type=FileType("r"),
        required=True,
        help="Input CSV File to be used",
    )
    args = parser.parse_args()
    data = read_csv(args.input_file.name)
    a = DashboardApplication(data)
    a.run()
