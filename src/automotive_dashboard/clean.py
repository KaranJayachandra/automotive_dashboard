from sys import argv
from numpy import sort, nan
from argparse import ArgumentParser, FileType
from logging import info, debug, basicConfig, INFO
from pandas import DataFrame, to_datetime, read_csv


def check_output() -> str:
    try:
        location = argv[2]
        return location
    except:
        raise ValueError(f"Output location required!")


def clean_make(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "make" column')
    info(f"Initial unique columns count: {len(data['make'].unique())}")
    debug(sort(data["make"].astype(str).unique()))
    data["make"] = data["make"].map(lambda x: str(x).replace(" truck", ""))
    data["make"] = data["make"].map(lambda x: str(x).replace(" tk", ""))
    data["make"] = data["make"].map(lambda x: str(x).capitalize())
    data.loc[data["make"] == "Chev", "make"] = "Chevrolet"
    data.loc[data["make"] == "Mercedes", "make"] = "Mercedes-benz"
    data.loc[data["make"] == "Mercedes-b", "make"] = "Mercedes-benz"
    data.loc[data["make"] == "Dot", "make"] = nan
    data.loc[data["make"] == "Nan", "make"] = nan
    data.loc[data["make"] == "Bmw", "make"] = "BMW"
    data.loc[data["make"] == "Kia", "make"] = "KIA"
    data.loc[data["make"] == "Ram", "make"] = "RAM"
    data.loc[data["make"] == "Geo", "make"] = "GEO"
    data.loc[data["make"] == "Gmc", "make"] = "GMC"
    data.loc[data["make"] == "Saab", "make"] = "SAAB"
    data.loc[data["make"] == "Vw", "make"] = "Volkswagen"
    info(f"Final unique columns count: {len(data['make'].unique())}")
    debug(sort(data["make"].astype(str).unique()))
    return data


def clean_transmission(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "transmission" column')
    info(f"Initial unique columns count: {len(data['transmission'].unique())}")
    debug(sort(data["transmission"].astype(str).unique()))
    data["transmission"] = data["transmission"].map(lambda x: str(x).capitalize())
    data.loc[data["transmission"] == "Sedan", "transmission"] = nan
    data.loc[data["transmission"] == "Nan", "transmission"] = nan
    info(f"Final unique columns count: {len(data['transmission'].unique())}")
    debug(sort(data["transmission"].astype(str).unique()))
    return data


def clean_color(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "color" column')
    info(f"Initial unique columns count: {len(data['color'].unique())}")
    debug(sort(data["color"].astype(str).unique()))
    data["color"] = data["color"].map(lambda x: str(x).capitalize())
    data.loc[data["color"].astype(str).str.isnumeric(), "color"] = nan
    data.loc[data["color"] == "Off-white", "color"] = "White"
    data.loc[data["color"] == "—", "color"] = nan
    info(f"Final unique columns count: {len(data['color'].unique())}")
    debug(sort(data["color"].astype(str).unique()))
    return data


def clean_interior(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "interior" column')
    info(f"Initial unique columns count: {len(data['interior'].unique())}")
    debug(sort(data["interior"].astype(str).unique()))
    data["interior"] = data["interior"].map(lambda x: str(x).capitalize())
    data.loc[data["interior"] == "Off-white", "interior"] = "White"
    data.loc[data["interior"] == "Nan", "interior"] = nan
    data.loc[data["interior"] == "—", "interior"] = nan
    info(f"Final unique columns count: {len(data['interior'].unique())}")
    debug(sort(data["interior"].astype(str).unique()))
    return data


def clean_state(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "state" column')
    info(f"Initial unique columns count: {len(data['state'].unique())}")
    debug(sort(data["state"].astype(str).unique()))
    data["state"] = data["state"].map(lambda x: str(x).capitalize())
    data.loc[data["state"].astype(str).str.len() > 2, "state"] = nan
    data["state"] = data["state"].map(lambda x: str(x).upper())
    info(f"Final unique columns count: {len(data['state'].unique())}")
    debug(sort(data["state"].astype(str).unique()))
    return data


def clean_body(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "body" column')
    info(f"Initial unique columns count: {len(data['body'].unique())}")
    debug(sort(data["body"].astype(str).unique()))
    data["body"] = data["body"].map(lambda x: str(x).lower())
    data.loc[data["body"].astype(str).str.contains("convertible"), "body"] = (
        "Convertible"
    )
    data.loc[data["body"].astype(str).str.contains("wagon"), "body"] = "Wagon"
    data.loc[data["body"].astype(str).str.contains("suv"), "body"] = "SUV"
    data.loc[data["body"].astype(str).str.contains("sedan"), "body"] = "Sedan"
    data.loc[data["body"].astype(str).str.contains("coupe"), "body"] = "Coupe"
    data.loc[data["body"].astype(str).str.contains("cab"), "body"] = "Cab"
    data.loc[data["body"].astype(str).str.contains("van"), "body"] = "Van"
    data.loc[data["body"] == "koup", "body"] = "Coupe"
    data.loc[data["body"] == "supercrew", "body"] = nan
    data.loc[data["body"] == "navitgation", "body"] = nan
    data.loc[data["body"] == "Nan", "body"] = nan
    data["body"] = data["body"].map(lambda x: str(x).capitalize())
    data.loc[data["body"] == "Suv", "body"] = "SUV"
    info(f"Final unique columns count: {len(data['body'].unique())}")
    debug(sort(data["body"].astype(str).unique()))
    return data


def clean_year(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "year" column')
    data["year"] = to_datetime(data["year"], format="%Y")
    return data


def clean_sale_date(data: DataFrame) -> DataFrame:
    info(f'Cleaning the "sale date" column')
    data["saledate"] = data["saledate"].map(lambda x: str(x).replace("GMT", ""))
    data["saledate"] = data["saledate"].map(lambda x: str(x)[:-6])
    data["saledate"] = to_datetime(
        data["saledate"], format="%a %b %d %Y %H:%M:%S %z", utc=True
    )
    return data


def remove_columns(data: DataFrame) -> DataFrame:
    data = data.drop(columns=["trim", "vin", "model", "seller"])
    return data


def rename_columns(data: DataFrame) -> DataFrame:
    data.rename(
        columns={
            "year": "Year",
            "make": "Manufacturer",
            "body": "Type",
            "transmission": "Transmission",
            "state": "State",
            "condition": "Rating",
            "odometer": "Mileage",
            "color": "Color",
            "interior": "Interior",
            "mmr": "Recommended Price",
            "sellingprice": "Selling Price",
            "saledate": "Sale Date",
        },
        inplace=True,
    )
    return data


def clean_data(input: DataFrame) -> DataFrame:
    data = clean_make(input)
    data = clean_transmission(data)
    data = clean_color(data)
    data = clean_interior(data)
    data = clean_state(data)
    data = clean_body(data)
    data = clean_year(data)
    data = clean_sale_date(data)
    data = remove_columns(data)
    data = rename_columns(data)
    data.sort_values(by="Sale Date", inplace=True)
    return data


def main() -> None:
    parser = ArgumentParser(
        prog="kaggle_data_clean",
        description="Clean up the data available at Kaggle to work with Dash application",
        epilog="Author: Karan Jayachandra",
    )
    parser.add_argument(
        "-i",
        "--input_file",
        type=FileType("r"),
        required=True,
        help="Input CSV File to be Cleaned",
    )
    parser.add_argument("-o", "--output_file", help="Input CSV File to be Cleaned")
    parser.add_argument(
        "-v",
        "--verbose",
        help="Be verbose",
        action="store_const",
        dest="loglevel",
        const=INFO,
    )
    args = parser.parse_args()
    basicConfig(level=args.loglevel)
    data = read_csv(args.input_file)
    info(f"Reading from location: {args.input_file}")
    if args.output_file is None:
        args.output_file = "clean_" + args.input_file.name
    info(f"Writing to location: {args.output_file}")
    info(f"The Data Types in the Data are: \n {data.dtypes}")
    for column in data:
        column_data = data[column]
        info(f"Column Name: {column}, Unique Values: {len(column_data.unique())}")
    cleaned_data = clean_data(data)
    cleaned_data.to_csv(args.output_file)
    print(data)
    print(f"Successfully cleaned data and stored at: {args.output_file}")


if __name__ == "__main__":
    main()
